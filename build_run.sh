#!/bin/bash

# To see the full tutorial: https://bartek-blog.github.io/nginx/node/javascript/2019/12/15/nginx-hello-world.html

docker build --tag=debiannginx . &&
docker run --detach --publish=5001:80 --name=debiannginx debiannginx &&
echo "Build and Run successful! Navigate to localhost:5001 in your browser to see your NGINX hello world html page!"
